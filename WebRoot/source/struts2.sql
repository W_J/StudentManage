/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50548
Source Host           : 127.0.0.1:3306
Source Database       : struts2

Target Server Type    : MYSQL
Target Server Version : 50548
File Encoding         : 65001

Date: 2017-01-07 18:28:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `CID` int(11) NOT NULL AUTO_INCREMENT,
  `GRADE` varchar(50) NOT NULL,
  `CLASSINFO` varchar(50) NOT NULL,
  `DEPARTMENT` varchar(50) NOT NULL,
  PRIMARY KEY (`CID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class
-- ----------------------------
INSERT INTO `class` VALUES ('6', '14级', '2班', '美术学院');
INSERT INTO `class` VALUES ('10', '14级', '1班', '经管学院');
INSERT INTO `class` VALUES ('12', '14级', '5班', '软件学院');
INSERT INTO `class` VALUES ('13', '21萨法', '啊嘎嘎', '在v啊');
INSERT INTO `class` VALUES ('14', '算法撒', '1啊是否是否', '哥仨个');
INSERT INTO `class` VALUES ('15', '在vDS', '松冈锭司', '梵蒂冈');
INSERT INTO `class` VALUES ('16', '15', '14', '1');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `CID` int(11) NOT NULL AUTO_INCREMENT,
  `CNAME` varchar(50) NOT NULL,
  `CL_ID` int(10) DEFAULT NULL,
  PRIMARY KEY (`CID`),
  KEY `FK_CLASS_CL_ID` (`CL_ID`),
  CONSTRAINT `FK_CLASS_CL_ID` FOREIGN KEY (`CL_ID`) REFERENCES `class` (`CID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', 'J2EE企业级', '12');
INSERT INTO `course` VALUES ('2', '大型数据库', '12');
INSERT INTO `course` VALUES ('5', '移动互联网技术基础', '12');
INSERT INTO `course` VALUES ('7', '操作系统', '12');
INSERT INTO `course` VALUES ('12', '软件工程', '12');
INSERT INTO `course` VALUES ('13', '网络技术', '12');
INSERT INTO `course` VALUES ('14', '交际礼仪与口才', '12');
INSERT INTO `course` VALUES ('15', '人体素描', '6');

-- ----------------------------
-- Table structure for mark_info
-- ----------------------------
DROP TABLE IF EXISTS `mark_info`;
CREATE TABLE `mark_info` (
  `MID` int(11) NOT NULL AUTO_INCREMENT,
  `S_ID` int(11) DEFAULT NULL,
  `C_ID` int(11) DEFAULT NULL,
  `MARK` varchar(20) NOT NULL,
  PRIMARY KEY (`MID`),
  KEY `FK_COURSE_CO_ID` (`C_ID`),
  KEY `FK_STUDENT_SID` (`S_ID`),
  CONSTRAINT `FK_COURSE_CO_ID` FOREIGN KEY (`C_ID`) REFERENCES `course` (`CID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_STUDENT_SID` FOREIGN KEY (`S_ID`) REFERENCES `student` (`SID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mark_info
-- ----------------------------
INSERT INTO `mark_info` VALUES ('1', '5', '1', '75分');
INSERT INTO `mark_info` VALUES ('2', '4', '7', '80分');
INSERT INTO `mark_info` VALUES ('3', '4', '1', '90');

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `SID` int(11) NOT NULL AUTO_INCREMENT,
  `STUNO` int(50) NOT NULL,
  `STUNAME` varchar(50) NOT NULL,
  `GENDER` varchar(20) NOT NULL,
  `CLASS_ID` int(10) DEFAULT NULL,
  PRIMARY KEY (`SID`),
  KEY `FK_CLASS_CID` (`CLASS_ID`),
  CONSTRAINT `FK_CLASS_CID` FOREIGN KEY (`CLASS_ID`) REFERENCES `class` (`CID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('4', '31470524', '王捷', '男', '6');
INSERT INTO `student` VALUES ('5', '31470530', '林建露', '男', '12');
INSERT INTO `student` VALUES ('6', '31470522', '陈财枫', '男', '6');
INSERT INTO `student` VALUES ('7', '31470541', '谢建兴', '男', '6');
INSERT INTO `student` VALUES ('12', '31470543', '田新风', '女', '12');
INSERT INTO `student` VALUES ('14', '31470211', '李娇娇', '男', '10');
INSERT INTO `student` VALUES ('15', '31471025', '阿飞色色', '男', '10');

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `TID` int(11) NOT NULL AUTO_INCREMENT,
  `TNAME` varchar(50) NOT NULL,
  `GENDER` varchar(50) NOT NULL,
  `TNO` int(50) NOT NULL,
  `CLASS_ID` int(11) DEFAULT NULL,
  `COURSE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`TID`),
  KEY `FK_COURSE_CID` (`COURSE_ID`),
  KEY `FK_CLASS_ID` (`CLASS_ID`),
  CONSTRAINT `FK_CLASS_ID` FOREIGN KEY (`CLASS_ID`) REFERENCES `class` (`CID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_COURSE_CID` FOREIGN KEY (`COURSE_ID`) REFERENCES `course` (`CID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('1', '肖剑飞', '男', '110501', null, '1');
INSERT INTO `teacher` VALUES ('2', '郑洪', '男', '314711', null, '5');
INSERT INTO `teacher` VALUES ('3', '叶明晔', '女', '317411', null, '2');
INSERT INTO `teacher` VALUES ('4', '翁公平', '男', '112402', null, '7');
INSERT INTO `teacher` VALUES ('5', '林平', '男', '314702', null, '12');
INSERT INTO `teacher` VALUES ('6', '柯财富', '男', '314705', null, '13');
INSERT INTO `teacher` VALUES ('7', '郑丽', '女', '14710', '10', '14');
INSERT INTO `teacher` VALUES ('8', '大幅改进', '男', '113', null, '5');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(20) NOT NULL,
  `USERPWD` varchar(20) NOT NULL,
  `BIRTHDAY` date NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `PAGEURL` varchar(50) NOT NULL,
  `PHO` varchar(50) NOT NULL,
  `USERGROUP_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_USER_GROUP_ID` (`USERGROUP_ID`),
  CONSTRAINT `FK_USER_GROUP_ID` FOREIGN KEY (`USERGROUP_ID`) REFERENCES `user_group` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('18', '王捷', '3673837', '2016-11-02', '42092@qq.com', 'http://localhost:8080', '/uploads/20170107175034484.jpg', '22');
INSERT INTO `user` VALUES ('22', '王捷', '1234', '2016-02-16', '420@qq.com', 'http://localhost:8080', '/uploads/20161228111058261.jpg', '23');
INSERT INTO `user` VALUES ('24', 'wangjie', '12134', '2016-02-16', '420@qq.com', 'http://localhost:8080', '/uploads/20161227122724536.jpg', '16');

-- ----------------------------
-- Table structure for user_group
-- ----------------------------
DROP TABLE IF EXISTS `user_group`;
CREATE TABLE `user_group` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_group
-- ----------------------------
INSERT INTO `user_group` VALUES ('15', '学生');
INSERT INTO `user_group` VALUES ('16', '教师');
INSERT INTO `user_group` VALUES ('22', '管理员');
INSERT INTO `user_group` VALUES ('23', '超级管理员');
