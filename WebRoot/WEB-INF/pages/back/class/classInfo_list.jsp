<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>班级信息</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
<script type="text/javascript">
	function turnPage(page)
	{
		document.getElementById("page").value = page;
		document.getElementById("frmSearch").submit();
	}
</script>
</head>
<body style="margin: 10px">
	<s:a namespace="/admin/class" action="add" class="btn btn-info btn-sm">添加</s:a>
	<s:form class="form-inline" id="frmSearch" namespace="/admin/class" action="index" method="post">
		<s:hidden name="page" id="page"></s:hidden>
		<table>
			<tr>
				<td>查询条件&nbsp;&nbsp;</td>
				<td>
					<%-- 系院：<s:textfield name="model.department" theme="simple"></s:textfield>
					年级：<s:textfield name="model.grade" theme="simple"></s:textfield>
						<s:submit value="查询" theme="simple"></s:submit> --%>
					<div class="from-group" style="margin-left: 65px">
						<label>系院：</label>
						<s:textfield class="form-control" name="model.department" theme="simple" placeholder="系院"/>	
						
						<label style="margin-left: 15px">年级：</label>
						<s:textfield class="form-control" name="model.grade" theme="simple" placeholder="年级"/>
						
						<s:submit value="查询" theme="simple" class="btn btn-info btn-sm	"></s:submit>					
				</div>
				</td>
			</tr>
		</table>
	</s:form>
	<table class="table table-striped table-hover">
		<thead>	
			<tr>
				<th>选择</th>
				<th>序列号</th>
				<th>系院</th>
				<th>年级</th>
				<th>班级</th>
				<th>操作</th>
			</tr>
		</thead>
		<s:iterator value="#pager.data" var="c" status="st">
			<tbody>	
				<tr>
					<td><input id="cb" type="checkbox" name="select" value="${c.cId }"/></td>
					<td>${st.index + 1 }</td>				
					<td>${c.department} </td>
					<td>${c.grade} </td>
					<td>${c.classInfo} </td>
					<td>
						<s:a namespace="/admin/class" action="delete"><s:param name="cId" value="#c.cId"/>删除</s:a>
					   |<s:a namespace="/admin/class" action="edit"><s:param name="cId" value="#c.cId"/>修改</s:a>
					</td>
				</tr>
			</tbody>		
		</s:iterator>
	</table>
	<table class="table table-striped">
		<tr>
			<td><a href="javascript:turnPage(1);">首页</a></td>
			<td><a href="javascript:turnPage(${pager.prevPage});">上一页</a></td>
			<td><a href="javascript:turnPage(${pager.nextPage});">下一页</a></td>
			<td><a href="javascript:turnPage(${pager.pages });">尾页</a></td>
		</tr>
	</table>
</body>
</html>