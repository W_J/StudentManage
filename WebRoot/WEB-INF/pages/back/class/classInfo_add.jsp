<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>班级信息</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
</head>
<body style="margin-left: 400px">
	<%-- <s:form class="form-inline" namespace="/admin/class" action="save" method="post">
		<s:hidden name="model.cId"></s:hidden>
		<s:textfield name="model.department" label="系院"></s:textfield>
		<s:textfield name="model.grade" label="年级"></s:textfield>
		<s:textfield name="model.classInfo" label="班级"></s:textfield>
		<s:submit value="保存"></s:submit> 
	    <s:reset value="取消"></s:reset>
	</s:form> --%>
	
	<form action="/admin/class/save" method="post">
		<div class="from-group">
			<s:hidden name="model.cId"></s:hidden>
		</div>
		<div class="from-group">
			<label>所属系院：</label>
			<s:textfield class="form-control" style="width:50%" name="model.department"></s:textfield>
		</div>
		<div class="from-group">
			<label>所属年级：</label>
			<s:textfield class="form-control" style="width:50%" name="model.grade"></s:textfield>
		</div>
		<div class="from-group">
			<label>所属班级：</label>
			<s:textfield class="form-control" style="width:50%" name="model.classInfo"></s:textfield>
		</div>
		<div class="from-group" style="margin:10px">
			<s:submit class="btn btn-info btn-sm" value="保存"></s:submit>
		</div>
	</form>
</body>
</html>