<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
</head>
<script type="text/javascript">
	function turnPage(page)
	{
		document.getElementById("page").value = page;
		document.getElementById("fremSearch").submit();
	}
</script>
<body style="margin: 10px">
<s:a namespace="/admin/teacher" action="add" class="btn btn-info btn-sm">添加</s:a>
<s:form class="form-inline" id="fremSearch" namespace="/admin/teacher" action="index" method="post">
	<s:hidden name="page" id="page"></s:hidden>
	<table>
		<tr>
			<td>查询条件&nbsp;&nbsp;</td>
			<td>
				<div class="from-group" style="margin-left: 65px">
						<label>教师名:</label>
						<s:textfield class="form-control" name="model.tName" theme="simple" placeholder="教师名"/>	
						
						<s:submit value="查询" theme="simple" class="btn btn-info btn-sm	"></s:submit>					
				</div>
			</td>
		</tr>
	</table>
</s:form>
<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>选择</th>
			<th>序号</th>
			<th>教师姓名</th>
			<th>性别</th>
			<th>教师号</th>
			<th>所属系院</th>
			<th>教授课程</th>
			<th>操作</th>
		</tr>
	</thead>
	<s:iterator value="#pager.data" var="t" status="st">
		<tbody>
			<tr>
				<td><input id="cb" type="checkbox" name="select" value="${t.tId }"/></td>
				<td>${st.index + 1 }</td>
				<td>${t.tName } </td>
				<td>${t.gender } </td>
				<td>${t.tNo } </td>
				<td><s:property value="classInfo.department"/> </td>
				<td><s:property value="course.cName"/></td>
				<td>
					<s:a namespace="/admin/teacher" action="delete"><s:param name="tId" value="#t.tId"/>刪除</s:a>
				|	<s:a namespace="/admin/teacher" action="edit"><s:param name="tId" value="#t.tId"/>修改</s:a>
				</td>
			</tr>
		</tbody>
	</s:iterator>
</table>
<table class="table table-striped">
	<tr>
		<td><a href="javascript:turnPage(1);">首页</a></td>
		<td><a href="javascript:turnPage(${pager.prevPage});">上一页</a></td>
		<td><a href="javascript:turnPage(${pager.nextPage});">下一页</a></td>
		<td><a href="javascript:turnPage(${pager.pages });">尾页</a></td>
	</tr>
</table>
</body>
</html>