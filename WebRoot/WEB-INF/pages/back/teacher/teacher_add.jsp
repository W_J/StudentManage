<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
</head>
<body style="margin-left: 400px">
	<%-- <s:form namespace="/admin/teacher" action="save" method="post">
		<s:hidden name="model.tId"></s:hidden>
		<s:textfield name="model.tName" label="教师姓名"></s:textfield>
		<s:textfield name="model.tNo" label="教师号"></s:textfield>
		<s:radio label="性别" name="model.gender" list="#{'男':'男','女':'女' }"></s:radio>
		<s:select label="所属系院" name="model.classInfo.cId" list="#classTypes" listKey="cId" listValue="department" />
		<s:select label="教授课程" name="model.course.cId" list="#courseTypes" listKey="cId" listValue="cName" />
		<s:submit value="保存"></s:submit> 
	    <s:reset value="取消"></s:reset>
	</s:form> --%>
	
	<form action="/admin/teacher/save" method="post">
		<div class="from-group">
			<s:hidden name="model.tId"></s:hidden>
		</div>
		<div class="from-group">
			<label>教师姓名:</label>
			<s:textfield class="form-control" style="width:50%" name="model.tName"></s:textfield>
		</div>
		<div class="from-group" style="margin-top: 10px">
			<label>教师号:</label>
			<s:textfield class="form-control" style="width:50%" name="model.tNo"></s:textfield>
		</div>
		<div class="from-group" style="margin-top: 10px">
			<label>性别:</label>
			<s:radio class="radio radio-inline" name="model.gender" list="#{'男':'男','女':'女' }"></s:radio>
		</div>
		<div class="from-group" style="margin-top: 10px">
			<label>所属系院:</label>
			<s:select class="form-control" style="width:50%" name="model.classInfo.cId" list="#classTypes" listKey="cId" listValue="department" />
		</div>
		<div class="from-group" style="margin-top: 10px">
			<label>教授课程:</label>
			<s:select class="form-control" style="width:50%" name="model.course.cId" list="#courseTypes" listKey="cId" listValue="cName" />
		</div>
		<div class="from-group" style="margin:10px">
			<s:submit class="btn btn-info btn-sm" value="保存"></s:submit>
		</div>
	
	</form>
</body>
</html>