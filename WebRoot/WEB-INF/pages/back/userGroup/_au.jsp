<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> 用户组信息</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
</head>
<body style="margin-left: 400px">
	<form action="/admin/userGroup/save" method="post">
		<div class="from-group">
			<s:hidden name="model.id"></s:hidden>
		</div>
		<div class="from-group">
			<label>组名称：</label>
			<s:textfield class="form-control" style="width:50%" name="model.name"></s:textfield>
		</div>
		<div class="from-group" style="margin:10px">
			<s:submit class="btn btn-info btn-sm" value="保存"></s:submit>
		</div>
	</form>
</body>
</html>