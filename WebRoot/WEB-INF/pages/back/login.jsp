<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户登入</title>
<link href="../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../css/signin.css" rel="stylesheet">
</head>
<body>
<!-- <form namesoace="/" action="/doLogin" method="post">
	登录名称：<input type="text" name="userName"><br />
	登录密码：<input type="password" name="userPwd"><br />
	验证码：<input type="text" name="code"><img src="/captche" ><br />
	<input type="submit" name="ok" value="登录">
	<input type="reset" name="cancel" value="取消"><br />
</form>
 -->
<div class="container">
      <form class="form-signin" role="form" namespace="/" action="doLogin" method="post">
        <h2 class="form-signin-heading">请登入</h2>
        <input type="text" name="userName" class="form-control" placeholder="用户名" required autofocus>
        <input type="password" name="userPwd" class="form-control" placeholder="密码" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> 记住我
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">登入</button>
      </form>
</div>
</body>
</html>