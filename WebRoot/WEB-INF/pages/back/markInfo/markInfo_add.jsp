<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
</head>
<body style="margin-left: 400px">
	<form action="/admin/markInfo/save" method="post" >
		<div class="from-group">
			<s:hidden name="model.mId"></s:hidden>
		</div>
		<div class="from-group">
			<label>学生姓名:</label>
			<s:select class="form-control" style="width:50%" name="model.student.sId" list="#studentTypes" listKey="sId" listValue="stuName" />
		</div>
		<div class="from-group">
			<label>所选课程:</label>
			<s:select class="form-control" style="width:50%" name="model.course.cId" list="#courseTypes" listKey="cId" listValue="cName" />
		</div>
		<div class="from-group">
			<label>成绩:</label>
			<s:textfield class="form-control" style="width:50%" name="model.mark"/>
		</div>
		<div class="from-group" style="margin-top: 10px">
			<s:submit class="btn btn-info btn-sm" value="保存"></s:submit>
		</div>
	</form>
</body>
</html>