<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>课程信息</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
</head>
<body style="margin-left: 400px">
	<form action="/admin/course/save" method="post" >
		<div class="from-group">
			<s:hidden name="model.cId"></s:hidden>
		</div>
		<div class="from-group">
			<label>所属系院：</label>
			<s:select class="form-control" style="width:25%" name="model.classInfo.cId" list="#classTypes" listKey="cId" listValue="department" />
		</div>
		<div class="from-group">
			<label>课程名：</label>
			<s:textfield class="form-control" style="width:25%" name="model.cName"/>
		</div>
		<div class="from-group" style="margin-top: 10px">
			<s:submit class="btn btn-info btn-sm" value="保存"></s:submit>
		</div>
	</form>
</body>
</html>