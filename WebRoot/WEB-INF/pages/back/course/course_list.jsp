<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
<script type="text/javascript">
	function turnPage(page)
	{
		document.getElementById("page").value = page;
		document.getElementById("frmSearch").submit();
	}
</script>
</head>
<body style="margin: 10px">
	<s:a namespace="/admin/course" action="add" class="btn btn-info btn-sm">添加</s:a>
	<s:form class="form-inline" id="frmSearch" namespace="/admin/course" action="index" method="post">
		<s:hidden name="page" id="page"></s:hidden>
		<table>
			<tr>
				<td>查询条件&nbsp;&nbsp;</td>
				<td>
					<div class="from-group" style="margin-left: 65px">
						<label>课程名：</label>
						<s:textfield class="form-control" name="model.cName" theme="simple" placeholder="课程名"/>
						<s:submit value="查询" theme="simple" class="btn btn-info btn-sm	"></s:submit>
					</div>
					<%-- <s:textfield name="model.cName" label="课程名" theme="simple" placeholder="姓名">课程名：</s:textfield> --%>
				</td>
			</tr>
		</table>
	</s:form>
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>选择</th>
				<th>序列号</th>
				<th>所属系院</th>
				<th>课程名</th>
				<th>操作</th>
			</tr>
		</thead>
		<s:iterator value="#pager.data" var="c" status="st">
		<tbody>
			<tr>
				<td><input id="cb" type="checkbox" name="select"/></td>
				<td>${st.index + 1 }</td>
				<td><s:property value="classInfo.department"/></td>				
				<td>${c.cName} </td>
				<td>
					<s:a namespace="/admin/course" action="delete"><s:param name="cId" value="#c.cId"/>删除</s:a>
				   |<s:a namespace="/admin/course" action="edit"><s:param name="cId" value="#c.cId"/>修改</s:a>
				</td>
			</tr>
		</tbody>		
		</s:iterator>
	</table>
	<table class="table table-striped">
		<tr>
			<td><a href="javascript:turnPage(1);">首页</a></td>
			<td><a href="javascript:turnPage(${pager.prevPage});">上一页</a></td>
			<td><a href="javascript:turnPage(${pager.nextPage});">下一页</a></td>
			<td><a href="javascript:turnPage(${pager.pages });">尾页</a></td>
		</tr>
	</table>
<script src="../../../../js/jquery.js"></script>
<script src="../../../../js/bootstrap.min.js"></script>	
</body>
</html>