<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
*
  {  
     margin: 0px; 
     padding: 0px
  }
#nav
  { 
     background-color:#eee; 
     width:100%; 
     height: 546px;
     margin:0 auto;
  }
ul
  {
     list-style: none;
  }
ul li
  {
     float: bottom; 
     line-height:40px;
     text-align:center; 
     position: relative;
  }
a
  {
     text-decoration: none; 
     color:black; 
     display: block; 
     padding:0 10px;
  }
a:HOVER 
  {   
     color: #FFF; 
     background-color: #666;
  }
ul li ul li
  { 
     float:none; 
     background-color: #FFF; 
     margin-top: 2px;
  }
ul li ul
  { 
     position: relative; 
     left:px; top:2px; 
     display: none;
  }
ul li ul li a:HOVER 
  {
     background-color: #06F;
  }
ul li:HOVER ul
  {
     display:block;
  }
</style>
</head>
<body>
	<%-- <table>
		<tr><td><s:a namespace="/admin/user" action="list" target="center">用户信息</s:a></td></tr>
		<tr><td><s:a namespace="/admin/userGroup" action="index" target="center">用户组</s:a></td></tr>
		<tr><td><s:a namespace="/admin/class" action="index" target="center">班级信息</s:a></td></tr>
		<tr><td><s:a namespace="/admin/student" action="list" target="center">学生信息</s:a></td></tr>
		<tr><td><s:a namespace="/admin/teacher" action="list" target="center">教师信息</s:a></td></tr>
		<tr><td><s:a namespace="/admin/course" action="list" target="center">课程</s:a></td></tr>
		<tr><td><s:a namespace="/admin/selectCourse" action="list" target="center">选课信息</s:a></td></tr>
	</table> --%>
<div id ="nav">
	  <ul>
	    <li><s:a namespace="/admin/user" action="list" target="center">用户信息</s:a>
	    	<ul>
	    		<li>
	    			<s:a namespace="/admin/userGroup" action="index" target="center">用户组</s:a>
	    		</li>
	    	</ul>
	    </li>
	    
	    <li><s:a namespace="/admin/student" action="list" target="center">学生信息</s:a>
	       <ul>
	         <li>
	         	<s:a namespace="/admin/class" action="index" target="center">班级信息</s:a>
	         </li>
	         <li>
	         	<s:a namespace="/admin/markInfo" action="index" target="center">选课信息</s:a>
	         </li>
	       </ul>
	    </li>
	    <li><s:a namespace="/admin/teacher" action="list" target="center">教师信息</s:a>
	    	<ul>
	    		<li>
	    			<s:a namespace="/admin/course" action="list" target="center">课程</s:a>
	    		</li>
	    	</ul>
	    </li>
	  </ul>
</div>
</body>
</html>