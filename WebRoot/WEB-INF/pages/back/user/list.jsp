<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
</head>
<script type="text/javascript">
	function turnPage(page)
	{
		document.getElementById("page").value = page;
		document.getElementById("fremSearch").submit();
	}
</script>
<body style="margin: 10px">
<s:a namespace="/admin/user" action="add" class="btn btn-info btn-sm">添加</s:a>
<s:form class="form-inline" id="fremSearch" namespace="/admin/user" action="list" method="post">
	<s:hidden name="page" id="page"></s:hidden>
	<table>
		<tr>
			<td>查询条件&nbsp;&nbsp;</td>
			<td>
				<div class="from-group" style="margin-left: 65px">
						<label>用户名：</label>
						<s:textfield class="form-control" name="model.userName" theme="simple" placeholder="用户名"/>	
						
						<label style="margin-left: 15px">邮箱：</label>
						<s:textfield class="form-control" name="model.email" theme="simple" placeholder="邮箱"/>
						
						<s:submit value="查询" theme="simple" class="btn btn-info btn-sm	"></s:submit>					
				</div>
				
				<%-- 用户名：<s:textfield name="model.userName" label="用户名" theme="simple"></s:textfield>
				邮箱：<s:textfield name="model.email" label="邮箱" theme="simple"></s:textfield>
					<s:submit value="查询" theme="simple"></s:submit> --%>
			</td>
		</tr>
	</table>
</s:form>
<table class="table table-striped table-hover">
	<thead>	
		<tr>
			<th>选择</th>
			<th>序号</th>
			<th>用户组</th>
			<th>姓名</th>
			<th>密码</th>
			<th>生日</th>
			<th>邮箱</th>
			<th>个人主页</th>
			<th>图片</th>
			<th>操作</th>
		</tr>
	</thead>
	<s:iterator value="#pager.data" var="u" status="st">
		<tbody>
			<tr>
				<td><input id="cb" type="checkbox" name="select" value="${u.id }"/></td>
				<td>${st.index + 1 }</td>
				<td><s:property value="userGroup.name"/></td>
				<td>${u.userName } </td>
				<td>${u.userPwd } </td>
				<td>${u.birthday} </td>
				<td>${u.email} </td>
				<td>${u.pageUrl} </td>
				<td><img src="${u.pho }" width="100"/></td>
				<td>
					<s:a namespace="/admin/user" action="delete"><s:param name="id" value="#u.id"/>刪除</s:a>
				|	<s:a namespace="/admin/user" action="edit"><s:param name="id" value="#u.id"/>修改</s:a>
				</td>
			</tr>
		</tbody>
	</s:iterator>
</table>
<table class="table table-striped">
	<tr>
		<td><a href="javascript:turnPage(1);">首页</a></td>
		<td><a href="javascript:turnPage(${pager.prevPage});">上一页</a></td>
		<td><a href="javascript:turnPage(${pager.nextPage});">下一页</a></td>
		<td><a href="javascript:turnPage(${pager.pages });">尾页</a></td>
	</tr>
</table>
<script src="../../../../js/jquery.js"></script>
<script src="../../../../js/bootstrap.min.js"></script>	
</body>
</html>