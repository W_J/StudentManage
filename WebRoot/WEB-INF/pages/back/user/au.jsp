<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
</head>
<body style="margin-left: 400px">
	<form action="/admin/user/save" method="post" enctype="multipart/form-data">
		<div class="from-group">
			<s:hidden name="model.id"></s:hidden>
		</div>
		<div class="from-group">
			<label>用户组名称：</label>
			<s:select class="form-control" style="width:50%" name="model.userGroup.id" list="#groupTypes" listKey="id" listValue="name" />
		</div>
		<div class="from-group">
			<label>姓名：</label>
			<s:textfield class="form-control" style="width:50%" name="model.userName"></s:textfield>
		</div>
		<div class="from-group">
			<label>密码：</label>
			<s:textfield class="form-control" style="width:50%" name="model.userPwd"></s:textfield>
		</div>
		<div class="from-group">
			<label>生日：</label>
			<s:textfield class="form-control" style="width:50%" name="model.birthday"></s:textfield>
		</div>
		<div class="from-group">
			<label>邮箱：</label>
			<s:textfield class="form-control" style="width:50%" name="model.email"></s:textfield>
		</div>
		<div class="from-group">
			<label>个人主页：</label>
			<s:textfield class="form-control" style="width:50%" name="model.pageUrl"></s:textfield>
		</div>
		<div class="from-group">
			<label>照片</label>
			<s:file class="form-control" style="width:50%" name="photo"></s:file>
		</div>
		<div class="from-group" style="margin:10px">
			<s:submit class="btn btn-info btn-sm" value="保存"></s:submit>
		</div>
	</form>
</body>
</html>