<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学生信息</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
<script type="text/javascript">
	function turnPage(page)
	{
		document.getElementById("page").value = page;
		document.getElementById("frmSearch").submit();
	}
</script>
</head>
<body style="margin: 10px">
	<s:a namespace="/admin/student" action="add" class="btn btn-info btn-sm">添加</s:a>
	<s:form class="form-inline" id="frmSearch" namespace="/admin/student" action="index" method="post">
		<s:hidden name="page" id="page"></s:hidden>
		<table>
			<tr>
				<td>查询条件&nbsp;&nbsp;</td>
				<td>
					<div class="from-group" style="margin-left: 65px">
						<label>学号：</label>
						<s:textfield class="form-control" name="model.stuName" theme="simple" placeholder="学号"/>							
						
						<s:submit value="查询" theme="simple" class="btn btn-info btn-sm	"></s:submit>					
					</div>
				</td>
			</tr>
		</table>
	</s:form>
	<table class="table table-striped table-hover">
		<thead>	
			<tr>
				<th>选择</th>
				<th>序列号</th>
				<th>姓名</th>
				<th>学号</th>
				<th>性别</th>
				<th>系院</th>
				<th>年级</th>
				<th>班级</th>
				<th>操作</th>
			</tr>
		</thead>
		<s:iterator value="#pager.data" var="s" status="st">
			<tbody>
				<tr>
					<td><input id="cb" type="checkbox" name="select" value="${s.sId }"/></td>
					<td>${st.index + 1 }</td>				
					<td>${s.stuName} </td>
					<td>${s.stuNo} </td>
					<td>${s.gender} </td>
					<td><s:property value="classInfo.department"/> </td>
					<td><s:property value="classInfo.grade"/></td>
					<td><s:property value="classInfo.classInfo"/></td>
					<td>
						<s:a namespace="/admin/student" action="delete"><s:param name="sId" value="#s.sId"/>删除</s:a>
					   |<s:a namespace="/admin/student" action="edit"><s:param name="sId" value="#s.sId"/>修改</s:a>
					</td>
				</tr>
			</tbody>		
		</s:iterator>
	</table>
	<table class="table table-striped">
		<tr>
			<td><a href="javascript:turnPage(1);">首页</a></td>
			<td><a href="javascript:turnPage(${pager.prevPage});">上一页</a></td>
			<td><a href="javascript:turnPage(${pager.nextPage});">下一页</a></td>
			<td><a href="javascript:turnPage(${pager.pages });">尾页</a></td>
		</tr>
	</table>
</body>
</html>