<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="../../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../../css/signin.css" rel="stylesheet">
</head>
<body style="margin-left: 400px">
	<%-- <s:form namespace="/admin/student" action="save" method="post">
		<s:hidden name="model.sId"></s:hidden>
		<s:textfield name="model.stuName" label="学生姓名"></s:textfield>
		<s:textfield name="model.stuNo" label="学号"></s:textfield>
		<s:radio label="性别" name="model.gender" list="#{'男':'男','女':'女' }"></s:radio>
		<s:select label="系院" name="model.classInfo.cId" list="#types" listKey="cId" listValue="department" />
		<s:select label="年级" name="model.classInfo.cId" list="#types" listKey="cId" listValue="grade" />
		<s:select label="班级" name="model.classInfo.cId" list="#types" listKey="cId" listValue="classInfo" />
		<s:submit value="保存"></s:submit> 
	    <s:reset value="取消"></s:reset>
	</s:form> --%>
	
	<form action="/admin/student/save" method="post">
		<div class="from-group">
			<s:hidden name="model.sId"></s:hidden>
		</div>
		<div class="from-group">
			<label>学生姓名:</label>
			<s:textfield class="form-control" style="width:50%" name="model.stuName"></s:textfield>
		</div>
		<div class="from-group">
			<label>学号:</label>
			<s:textfield class="form-control" style="width:50%" name="model.stuNo"></s:textfield>
		</div>
		<div class="from-group">
			<label>性别:</label>
			<s:radio class="radio radio-inline" name="model.gender" list="#{'男':'男','女':'女' }"></s:radio>
			<%-- <s:radio class="form-control" style="width:50%" name="model.gender" list="#{'男':'男','女':'女' }"></s:radio> --%>
		</div>
		<div class="from-group">
			<label>系院:</label>
			<s:select class="form-control" style="width:50%" name="model.classInfo.cId" list="#types" listKey="cId" listValue="department" />
		</div>
		<div class="from-group">
			<s:submit class="btn btn-info btn-sm" value="保存"></s:submit>
		</div>
	
	
	</form>
</body>
</html>