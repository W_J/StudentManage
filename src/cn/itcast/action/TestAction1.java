package cn.itcast.action;


import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;
@ParentPackage("struts-default")
@Namespace("/")
@Results({@Result(name="helloWorld",location="/test.jsp"),
	 //@Result(name="success1",location="/index.jsp"),
	 @Result(name="success",location="/index.jsp"),
    // @Result(name="success",location="/index.jsp")
})
public class TestAction1 extends ActionSupport {
	/*@Action(value="test")
	 public String test()
    {
		int[] k={1,2,3};
		if(k[0] == 1)
   	   		return SUCCESS;
   	   else
   		   	return "success1";
    }*/
	@Action(value="Test")
	public String hello()
	{
		return SUCCESS;
	}
	
	@Action(value="helloWorld")
	public String World()
	{
		return "helloWorld";
	}

}
