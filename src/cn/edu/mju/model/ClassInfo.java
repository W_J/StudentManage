package cn.edu.mju.model;

/**
 * 班级表
 * @author 王捷
 * @时间：2016年12月22日10:42:26
 *
 */
public class ClassInfo {
	
	private Integer cId;
	private String department;		//系院
	private String grade;			//年级
	private String classInfo;		//班级
	
	
	public ClassInfo(Integer cId, String department, String grade, String classInfo) {
		super();
		this.cId = cId;
		this.department = department;
		this.grade = grade;
		this.classInfo = classInfo;
	}


	public ClassInfo() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Integer getcId() {
		return cId;
	}


	public void setcId(Integer cId) {
		this.cId = cId;
	}


	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}


	public String getGrade() {
		return grade;
	}


	public void setGrade(String grade) {
		this.grade = grade;
	}


	public String getClassInfo() {
		return classInfo;
	}


	public void setClassInfo(String classInfo) {
		this.classInfo = classInfo;
	}
	
	
	
	
	
}
