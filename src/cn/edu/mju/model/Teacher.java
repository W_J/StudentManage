package cn.edu.mju.model;

public class Teacher {
	
	private Integer tId;
	private String tName;   //教师名
	private Integer tNo;	//教师号
	private String gender;  //性别
	private ClassInfo classInfo;   //与班级表关联
	private Course course;			//与课程表关联
	
	
	public Teacher(Integer tId, String tName, Integer tNo, String gender, ClassInfo classInfo, Course course) {
		super();
		this.tId = tId;
		this.tName = tName;
		this.tNo = tNo;
		this.gender = gender;
		this.classInfo = classInfo;
		this.course = course;
	}


	public Teacher() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Integer gettId() {
		return tId;
	}


	public void settId(Integer tId) {
		this.tId = tId;
	}


	public String gettName() {
		return tName;
	}


	public void settName(String tName) {
		this.tName = tName;
	}


	public Integer gettNo() {
		return tNo;
	}


	public void settNo(Integer tNo) {
		this.tNo = tNo;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public ClassInfo getClassInfo() {
		return classInfo;
	}


	public void setClassInfo(ClassInfo classInfo) {
		this.classInfo = classInfo;
	}


	public Course getCourse() {
		return course;
	}


	public void setCourse(Course course) {
		this.course = course;
	}
	
	
	
	
}
