package cn.edu.mju.model;

public class Student {
	
	private Integer sId;
	private String stuName;
	private Integer stuNo;
	private String gender;
	private ClassInfo classInfo;
	
	
	public Student(Integer sId, String stuName, Integer stuNo, String gender, ClassInfo classInfo) {
		super();
		this.sId = sId;
		this.stuName = stuName;
		this.stuNo = stuNo;
		this.gender = gender;
		this.classInfo = classInfo;
	}


	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Integer getsId() {
		return sId;
	}


	public void setsId(Integer sId) {
		this.sId = sId;
	}


	public String getStuName() {
		return stuName;
	}


	public void setStuName(String stuName) {
		this.stuName = stuName;
	}


	public Integer getStuNo() {
		return stuNo;
	}


	public void setStuNo(Integer stuNo) {
		this.stuNo = stuNo;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public ClassInfo getClassInfo() {
		return classInfo;
	}


	public void setClassInfo(ClassInfo classInfo) {
		this.classInfo = classInfo;
	}
	
	

}
