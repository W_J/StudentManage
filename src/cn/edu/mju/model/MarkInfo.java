package cn.edu.mju.model;

/**
 * 成绩信息
 * @author 王捷
 * @时间:2016年12月26日02:16:46
 */
public class MarkInfo {
	
	private Integer mId;
	private Student student;
	private Course course;
	private String mark;   		//成绩
	
	
	public MarkInfo(Integer mId, Student student, Course course, String mark) {
		super();
		this.mId = mId;
		this.student = student;
		this.course = course;
		this.mark = mark;
	}


	public MarkInfo() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Integer getmId() {
		return mId;
	}


	public void setmId(Integer mId) {
		this.mId = mId;
	}


	public Student getStudent() {
		return student;
	}


	public void setStudent(Student student) {
		this.student = student;
	}


	public Course getCourse() {
		return course;
	}


	public void setCourse(Course course) {
		this.course = course;
	}


	public String getMark() {
		return mark;
	}


	public void setMark(String mark) {
		this.mark = mark;
	}
	
	
	
	
	
}
