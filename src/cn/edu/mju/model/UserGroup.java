package cn.edu.mju.model;

public class UserGroup {
	private Integer id;
	private String name;
	
	
	
	public UserGroup() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public UserGroup(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
