package cn.edu.mju.model;

import java.sql.Date;

public class User {
	private Integer id;
	private String userName;
	private String userPwd;
	private Date birthday;
	private String email;
	private String pho;
	private String pageUrl;
	private UserGroup userGroup;
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public User(Integer id, String userName, String userPwd, Date birthday, String email, String pho, String pageUrl,
			UserGroup userGroup) {
		super();
		this.id = id;
		this.userName = userName;
		this.userPwd = userPwd;
		this.birthday = birthday;
		this.email = email;
		this.pho = pho;
		this.pageUrl = pageUrl;
		this.userGroup = userGroup;
	}
	



	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPho() {
		return pho;
	}
	public void setPho(String pho) {
		this.pho = pho;
	}
	public String getPageUrl() {
		return pageUrl;
	}
	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
	public UserGroup getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}
	
	
}
