package cn.edu.mju.model;


/**
 * model:课程
 * @author 王捷
 * @时间：2016年12月24日00:09:32
 */
public class Course {
	
	private Integer cId;
	private String cName;		//课程名
	private ClassInfo classInfo;  //
	
	


	public Course(Integer cId, String cName, ClassInfo classInfo) {
		super();
		this.cId = cId;
		this.cName = cName;
		this.classInfo = classInfo;
	}


	public Course() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Integer getcId() {
		return cId;
	}


	public void setcId(Integer cId) {
		this.cId = cId;
	}


	public String getcName() {
		return cName;
	}


	public void setcName(String cName) {
		this.cName = cName;
	}


	public ClassInfo getClassInfo() {
		return classInfo;
	}


	public void setClassInfo(ClassInfo classInfo) {
		this.classInfo = classInfo;
	}
	
	
	

}
