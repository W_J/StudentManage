package cn.edu.mju.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DbHelper {
	private static final Configuration config;
	private static final SessionFactory factory;
	static{
		config = new Configuration().configure();		//构建Configuration实例，加载hibernate.cfg.xml文件
		factory = config.buildSessionFactory();			//Configuration实例构建一个SessionFactory实例
	}
	
	public static Session getSession()
	{
		return factory.openSession();
	}
}
