package cn.edu.mju.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.edu.mju.model.User;

public class MainController extends ActionSupport implements ModelDriven<User> {
	private String code;
	private User user = new User();
	private String userName;
	
	/**
	 * 显示登录界面的Action
	 * @return
	 */
	public String login()
	{
		String hello = "Hello";
		ActionContext.getContext().put("hello", hello);

		return SUCCESS;
	}
	
	public String doLogin()
	{
		Object obj = ActionContext.getContext().getSession().get("code");
		String saveCode = null;
		if(obj !=null)
		{
			saveCode = obj.toString();
		}
		if("admin".equals(user.getUserName()) && "1234".equals(user.getUserPwd()))
		{
			ActionContext.getContext().getSession().put("user", "test");
			return SUCCESS;
		}
		else
			return "fail";
		
	}
	
	public String main()
	{
		/*
		List<User> lst = new ArrayList<User>();
		User user = new User("a1","pw1","男",Date.valueOf("2000-1-1"));
		lst.add(user);
		lst.add(new User("a2","pw2","男",Date.valueOf("2000-1-1")));
		lst.add(new User("a3","pw3","男",Date.valueOf("2000-1-1")));
		ActionContext.getContext().put("users", lst);
		ActionContext.getContext().put("info", lst);*/

		return "main";
	}
	public String top(){
		return SUCCESS;
	}
	
	public String menu(){
		return SUCCESS;
	}
	
	public String center(){
		return SUCCESS;
	}
	
	public String copyright(){
		return SUCCESS;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public User getModel() {
		// TODO Auto-generated method stub
		return user;
	}
}
