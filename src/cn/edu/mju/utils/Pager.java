package cn.edu.mju.utils;

import java.util.List;

public class Pager {
		
	private int page;
	private int pageSize = 6;
	private int pages;
	private int rows;
	private int prevPage;
	private int nextPage;
	private List data;
	
	public void setPage(int page) {
		this.page = page;
	}
	public int getPage() {
		return page;
	}
	public void setPage(Integer page) {
		if(page == null)
		{
			page = 1;
		}
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		
		pages = rows / pageSize;
		if(pages * pageSize < rows)
		{
			pages = pages + 1;
		}
		this.rows = rows;
	}
	public int getPrevPage() {
		
		prevPage = page - 1;
		if(prevPage < 1)
		{
			prevPage = 1;
		}
		return prevPage;
	}
	public void setPrevPage(int prevPage) {
		this.prevPage = prevPage;
	}
	public int getNextPage() {
		
		nextPage = page + 1;
		if(nextPage > pages)
		{
			nextPage = pages;
		}
		return nextPage;
	}
	public void setNextPage(int nextPage) {
		
		this.nextPage = nextPage;
	}
	public List getData() {
		return data;
	}
	public void setData(List data) {
		this.data = data;
	}
	
	
	
}


