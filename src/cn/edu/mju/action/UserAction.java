package cn.edu.mju.action;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.apache.struts2.convention.annotation.Result;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.validator.annotations.DateRangeFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RegexFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;

import cn.edu.mju.model.User;
import cn.edu.mju.model.UserGroup;
import cn.edu.mju.service.IUserGroupService;
import cn.edu.mju.service.IUserService;
import cn.edu.mju.service.impl.UserGroupService;
import cn.edu.mju.service.impl.UserService;
import cn.edu.mju.utils.Pager;
@ParentPackage("struts-default")
@Namespace("/admin/user")
@Results(value = {
		@Result(name="list",location="/WEB-INF/pages/back/user/list.jsp"),
		@Result(name="au",location="/WEB-INF/pages/back/user/au.jsp"),
		@Result(name="delete",location="/WEB-INF/pages/back/user/list.jsp"),
		@Result(name="input",location="/WEB-INF/pages/back/user/au.jsp"),
		@Result(name="ok",type="redirectAction",params={"namespace","/admin/user","actionName","list"})
				
})
 public class UserAction extends ActionSupport implements ModelDriven<User> {
	
	private User user = new User();
	private UserGroup userGroup = new UserGroup();
	private IUserService userService = new UserService();
	private IUserGroupService userGroupService = new UserGroupService();
	
	private File photo;
	private String photoFileName;
	private String photoContentType;
	private Integer page;
	
	@Action("add")
	public String add()
	{
		List<UserGroup> groupTypes = userGroupService.findAll(userGroup);
		ActionContext.getContext().put("groupTypes", groupTypes);
	    return "au";
	}
	
	@Action(value = "save", interceptorRefs = { @InterceptorRef(value = "defaultStack", params = {
			"fileUpload.maximumSize", "4194304", "fileUpload.allowedTypes", "image/jpeg" }) })
	public String save()
	{	
		try {
			photoFileName = new String(photoFileName.getBytes("ISO-8859-1"), "UTF-8");// 解决中文乱码
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("FileName=" + photoFileName);
		System.out.println("Content Type=" + photoContentType);
		if (photo != null) 
		{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			String fname = sdf.format(System.currentTimeMillis());
			int pos = photoFileName.lastIndexOf(".");
			fname = fname + photoFileName.substring(pos);
			String destFileName = ServletActionContext.getRequest().getRealPath("uploads") + File.separator + fname;
			File dest = new File(destFileName);
			try {
				FileUtils.copyFile(photo, dest);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			String url = ServletActionContext.getRequest().getContextPath();
			url = url + "/";
			url = url + "uploads/" + fname;
			System.out.println("url=" + url + destFileName);
			user.setPho(url);
		}
		userService.save(user);
		return "ok";
	}
	
	
	@Action("delete")
	public String delete()
	{
		if(user != null && user.getId() != null)
		{
			userService.delete(user.getId());
		}
		return "ok";
	}
	
	@Action("edit")
	public String edit()
	{
		List<UserGroup> groupTypes = userGroupService.findAll(userGroup);
		ActionContext.getContext().put("groupTypes", groupTypes);
		if(user != null)
		{
			user = userService.findById(user.getId());
		}
	    return "au";
	}
	
	@Action("list")
	public String list()
	{
		Pager pager = userService.findAll(page,user);
		ActionContext.getContext().put("pager", pager);
	    return "list";
	}

	@Override
	public User getModel() {
		// TODO Auto-generated method stub
		return user;
	}

	public File getPhoto() {
		return photo;
	}

	public void setPhoto(File photo) {
		this.photo = photo;
	}

	public String getPhotoFileName() {
		return photoFileName;
	}

	public void setPhotoFileName(String photoFileName) {
		this.photoFileName = photoFileName;
	}

	public String getPhotoContentType() {
		return photoContentType;
	}

	public void setPhotoContentType(String photoContentType) {
		this.photoContentType = photoContentType;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}
	
	
}
