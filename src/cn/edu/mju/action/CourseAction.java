package cn.edu.mju.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.edu.mju.model.ClassInfo;
import cn.edu.mju.model.Course;
import cn.edu.mju.service.IClassInfoService;
import cn.edu.mju.service.ICourseService;
import cn.edu.mju.service.impl.ClassInfoService;
import cn.edu.mju.service.impl.CourseService;
import cn.edu.mju.utils.Pager;

@ParentPackage("struts-default")
@Namespace("/admin/course")
@Results(value = { 
		@Result(name="index",location="/WEB-INF/pages/back/course/course_list.jsp"),
		@Result(name="add",location="/WEB-INF/pages/back/course/course_add.jsp"),
		@Result(name="save",type="redirectAction",params={"namespace","/admin/course","actionName","index"})
		})
public class CourseAction extends ActionSupport implements ModelDriven<Course>{
	
	private Course course = new Course();
	private ClassInfo classInfo = new ClassInfo();
	/*private Pager pager = new Pager();*/
	private ICourseService courseSevice = new CourseService();
	private IClassInfoService classInfoService = new ClassInfoService();
	private Integer page;	
	
	@Action("index")
	public String index()
	{
		Pager pager = courseSevice.findPager(page, course);
		ActionContext.getContext().put("pager", pager);
		return "index";
	}
	
	
	@Action("add")
	public String add()
	{
		List<ClassInfo> classTypes = classInfoService.findAll(classInfo);
		ActionContext.getContext().put("classTypes", classTypes);
		return "add";
	}
	
	
	@Action("save")
	public String save()
	{
		courseSevice.save(course);
		return "save";
	}
	
	
	@Action("edit")
	public String edit()
	{
		List<ClassInfo> classTypes = classInfoService.findAll(classInfo);
		ActionContext.getContext().put("classTypes", classTypes);
		if(course != null )
		{
			course = courseSevice.findById(course.getcId());
		}
		return "add";
	}
	
	
	@Action("delete")
	public String delete()
	{
		if(course != null && course.getcId() > 0)
		{
			courseSevice.delete(course.getcId());
		}
		return "save";
	}


	@Override
	public Course getModel() {
		// TODO Auto-generated method stub
		return course;
	}


	public Integer getPage() {
		return page;
	}


	public void setPage(Integer page) {
		this.page = page;
	}
	
	
}
