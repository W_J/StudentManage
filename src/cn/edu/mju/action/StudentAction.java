package cn.edu.mju.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.edu.mju.model.ClassInfo;
import cn.edu.mju.model.Student;
import cn.edu.mju.service.IClassInfoService;
import cn.edu.mju.service.IStudentService;
import cn.edu.mju.service.impl.ClassInfoService;
import cn.edu.mju.service.impl.StudentService;
import cn.edu.mju.utils.Pager;

@ParentPackage("struts-default")
@Namespace("/admin/student")
@Results(value = { 
		@Result(name="index",location="/WEB-INF/pages/back/student/student_list.jsp"),
		@Result(name="add",location="/WEB-INF/pages/back/student/student_add.jsp"),
		@Result(name="input",location="/WEB-INF/pages/back/student/student_add.jsp"),
		@Result(name="save",type="redirectAction",params={"namespace","/admin/student","actionName","index"})
		})
public class StudentAction extends ActionSupport implements ModelDriven<Student>{
	
	private Student student = new Student();
	private ClassInfo classInfo = new ClassInfo();
	private IStudentService studentService = new StudentService();
	private IClassInfoService classInfoService = new ClassInfoService();
	private Integer page;
	private Map<String, Object> department;

	@Action("index")
	@SkipValidation
	public String index()
	{
		Pager pager = studentService.findAll(page,student);
		ActionContext.getContext().put("pager", pager);
		return "index";
	}
	
	
	
	
	@Action("add")
	@SkipValidation
	public String add()
	{
		List<ClassInfo> classTypes = classInfoService.findAll(classInfo);
		ActionContext.getContext().put("types", classTypes);
		classInfo.setcId(0);
		return "add";
	}
	
	
	
	@Action("save")
	public String dosave()
	{	
		department = ActionContext.getContext().getParameters();
		System.out.println("___________" + department);
		studentService.save(student);
		return "save";
	}
	
	
	
	@Action("delete")
	@SkipValidation
	public String delete()
	{	
		if(student != null && student.getsId() > 0)
		{
			studentService.delete(student.getsId());
		}
		return "save";
	}
	
	
	@Action("edit")
	@SkipValidation
	public String edit()
	{	
		List<ClassInfo> types = classInfoService.findAll(classInfo);
		ActionContext.getContext().put("types", types);
		if(student != null)
		{
			student = studentService.findById(student.getsId());
		}
		return "add";
	}




	@Override
	public Student getModel() {
		// TODO Auto-generated method stub
		return student;
	}

	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}

	public ClassInfo getClassInfo() {
		return classInfo;
	}

	public void setClassInfo(ClassInfo classInfo) {
		this.classInfo = classInfo;
	}

	
	
}
