package cn.edu.mju.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.edu.mju.model.Course;
import cn.edu.mju.model.MarkInfo;
import cn.edu.mju.model.Student;
import cn.edu.mju.service.ICourseService;
import cn.edu.mju.service.IMarkInfoService;
import cn.edu.mju.service.IStudentService;
import cn.edu.mju.service.impl.CourseService;
import cn.edu.mju.service.impl.MarkInfoService;
import cn.edu.mju.service.impl.StudentService;
import cn.edu.mju.utils.Pager;

@ParentPackage("struts-default")
@Namespace("/admin/markInfo")
@Results(value = { 
		@Result(name="index",location="/WEB-INF/pages/back/markInfo/markInfo_list.jsp"),
		@Result(name="add",location="/WEB-INF/pages/back/markInfo/markInfo_add.jsp"),
		@Result(name="save",type="redirectAction",params={"namespace","/admin/markInfo","actionName","index"})
		})
public class MarkInfoAction extends ActionSupport implements ModelDriven<MarkInfo>{
	
	private MarkInfo markInfo = new MarkInfo();
	private Student student = new Student();
	private Course course = new Course();
	private Pager pager = new Pager();
	private IMarkInfoService markInfoService = new MarkInfoService();
	private IStudentService studentService = new StudentService();
	private ICourseService courseService = new CourseService();
	private Integer page;
	
	@Action("index")
	public String index()
	{
		pager = markInfoService.findPager(page, markInfo);
		ActionContext.getContext().put("pager", pager);
		return "index";
	}
	
	@Action("add")
	public String add()
	{
		List<Student> studentTypes = studentService.findAll(student);
		ActionContext.getContext().put("studentTypes", studentTypes);
		List<Course> courseTypes = courseService.findAll(course);
		ActionContext.getContext().put("courseTypes", courseTypes);

		return "add";
	}
	
	@Action("save")
	public String save()
	{
		markInfoService.save(markInfo);
		return "save";
	}
	
	@Action("delete")
	public String delete()
	{
		if(markInfo != null && markInfo.getmId() > 0)
		{
			markInfoService.delete(markInfo.getmId());
		}
		return "save";
	}
	
	@Action("edit")
	public String edit()
	{
		List<Student> studentTypes = studentService.findAll(student);
		ActionContext.getContext().put("studentTypes", studentTypes);
		List<Course> courseTypes = courseService.findAll(course);
		ActionContext.getContext().put("courseTypes", courseTypes);
		if(markInfo != null)
		{
			markInfo = markInfoService.findById(markInfo.getmId());
		}
		return "add";
	}

	@Override
	public MarkInfo getModel() {
		// TODO Auto-generated method stub
		return markInfo;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}
	
	
}
