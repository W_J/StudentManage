package cn.edu.mju.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.config.entities.ActionConfig;

import cn.edu.mju.model.UserGroup;
import cn.edu.mju.service.IUserGroupService;
import cn.edu.mju.service.impl.UserGroupService;
import cn.edu.mju.utils.Pager;
@ParentPackage("struts-default")
@Namespace("/admin/userGroup")
@Results(value = { 
		@Result(name="index",location="/WEB-INF/pages/back/userGroup/_index.jsp"),
		@Result(name="add",location="/WEB-INF/pages/back/userGroup/_au.jsp"),
		@Result(name="save",type="redirectAction",params={"namespace","/admin/userGroup","actionName","index"}),
		//@Result(name="delete",type="redirectAction",params={"namespace","/admin/userGroup","actionName","index"})
		})
public class UserGroupAction extends ActionSupport implements ModelDriven<UserGroup>{
	private UserGroup userGroup = new UserGroup();
	private IUserGroupService userGroupService = new UserGroupService();
	private Pager pager = new Pager();
	private String ids;
	private Integer page;
	
	@Action("index")
	public String index()
	{
		/*List<UserGroup> list = userGroupService.findAll(userGroup);
		ActionContext.getContext().put("pager", list);*/
		
		pager = userGroupService.findAll(page, userGroup);
		ActionContext.getContext().put("pager", pager);
		return "index";
	}
	@Action("add")
	public String add()
	{
		/*if(userGroup != null && userGroup.getId() != null)
		{
			userGroupService.findById(userGroup.getId());
		}*/
		return "add";
	}
	@Action("save")
	public String save()
	{
		userGroupService.save(userGroup);
		return "save";
	}
	
	@Action("delete")
	public String delete()
	{	
		if( ids != null)
		{
			String[] delIds = ids.split(","); 
			for(String delId:delIds)
			{	
				try {
					Integer id = Integer.parseInt(delId);
					userGroupService.deleteById(id);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		}
		return "save";
	}
	
	@Action("edit")
	public String edit()
	{
		System.out.println("++++++++++++++++" + userGroup.getId());
		if(userGroup.getId()!= null)
		{
			userGroup = userGroupService.findById(userGroup.getId());
		}
		else
		{
			System.out.println("name == null");
		}
		return "add";
	}
	
	@Override
	public UserGroup getModel() {
		// TODO Auto-generated method stub
		return userGroup;
	}
	
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	
	
}
