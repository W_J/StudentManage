package cn.edu.mju.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.edu.mju.model.User;
import cn.edu.mju.service.IUserService;
import cn.edu.mju.service.impl.UserService;


public class LoginAction extends ActionSupport implements ModelDriven<User>{
	
	private User user = new User();
	private IUserService userService = new UserService();
	
	public String index()
	{
		return "login";
	}
	
	public String doLogin()
	{
		List<User> userName = userService.findName();
		List<User> userPwd = userService.findPwd();
		
		if(userName.contains(user.getUserName()) && userPwd.contains(user.getUserPwd()))
		{
			return "success";
		}
		else
		{
			return "fail";
		}
	}
	
	public String loginOut()
	{
		ActionContext.getContext().getSession().clear();
		return "out";
	}

	@Override
	public User getModel() {
		// TODO Auto-generated method stub
		return user;
	}
}
