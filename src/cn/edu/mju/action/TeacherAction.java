package cn.edu.mju.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.edu.mju.model.ClassInfo;
import cn.edu.mju.model.Course;
import cn.edu.mju.model.Teacher;
import cn.edu.mju.service.IClassInfoService;
import cn.edu.mju.service.ICourseService;
import cn.edu.mju.service.ITeacherService;
import cn.edu.mju.service.impl.ClassInfoService;
import cn.edu.mju.service.impl.CourseService;
import cn.edu.mju.service.impl.TeacherService;
import cn.edu.mju.utils.Pager;

@ParentPackage("struts-default")
@Namespace("/admin/teacher")
@Results(value = { 
		@Result(name="index",location="/WEB-INF/pages/back/teacher/teacher_list.jsp"),
		@Result(name="add",location="/WEB-INF/pages/back/teacher/teacher_add.jsp"),
		@Result(name="save",type="redirectAction",params={"namespace","/admin/teacher","actionName","index"})
		})
public class TeacherAction extends ActionSupport implements ModelDriven<Teacher>{
	
	private Teacher teacher = new Teacher();
	private ClassInfo classInfo = new ClassInfo();
	private Course course = new Course();
	private ITeacherService teacherService = new TeacherService();
	private IClassInfoService classInfoService = new ClassInfoService();
	private ICourseService courseService = new CourseService();
	private Pager pager = new Pager();
	private Integer page;
	
	@Action("index")
	public String index()
	{
		pager = teacherService.findPager(page, teacher);
		ActionContext.getContext().put("pager", pager);
		return "index";
	}
	
	
	@Action("add")
	public String add()
	{
		List<ClassInfo> classTypes = classInfoService.findAll(classInfo);
		ActionContext.getContext().put("classTypes", classTypes);
		
		List<Course> courseTypes = courseService.findAll(course);
		ActionContext.getContext().put("courseTypes", courseTypes);
		
		return "add";
	}
	
	
	@Action("save")
	public String save()
	{
		teacherService.save(teacher);
		return "save";
	}
	
	
	@Action("edit")
	public String edit()
	{
		List<ClassInfo> classTypes = classInfoService.findAll(classInfo);
		ActionContext.getContext().put("classTypes", classTypes);
		List<Course> courseTypes = courseService.findAll(course);
		ActionContext.getContext().put("courseTypes", courseTypes);
		if(teacher != null)
		{
			teacher = teacherService.findById(teacher.gettId());
		}
		return "add";
	}
	
	
	@Action("delete")
	public String delete()
	{
		if(teacher != null && teacher.gettId() > 0)
		{
			teacherService.delete(teacher.gettId());
		}
		return "save";
	}


	@Override
	public Teacher getModel() {
		// TODO Auto-generated method stub
		return teacher;
	}


	public Integer getPage() {
		return page;
	}


	public void setPage(Integer page) {
		this.page = page;
	}
	
}
