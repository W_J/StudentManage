package cn.edu.mju.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.edu.mju.model.ClassInfo;
import cn.edu.mju.service.IClassInfoService;
import cn.edu.mju.service.impl.ClassInfoService;
import cn.edu.mju.utils.Pager;

@ParentPackage("struts-default")
@Namespace("/admin/class")
@Results(value = { 
		@Result(name="index",location="/WEB-INF/pages/back/class/classInfo_list.jsp"),
		@Result(name="add",location="/WEB-INF/pages/back/class/classInfo_add.jsp"),
		@Result(name="save",type="redirectAction",params={"namespace","/admin/class","actionName","index"})
		})
public class ClassInfoAction extends ActionSupport implements ModelDriven<ClassInfo>{
	
	
	private ClassInfo classInfo = new ClassInfo();
	private IClassInfoService classInfoService = new ClassInfoService();
	private Pager pager = new Pager();
	private Integer page;
	private Integer cId;
	
	@Action("index")
	public String index()
	{
		pager = classInfoService.findAll(page,classInfo);
		System.out.println("zshflan++++++++++" + pager.getData());
		ActionContext.getContext().put("pager", pager);
		return "index";
	}
	
	
	
	
	@Action("add")
	public String add()
	{
		
		return "add";
	}
	
	
	
	@Action("save")
	public String dosave()
	{
		classInfoService.save(classInfo);
		return "save";
	}
	
	
	
	@Action("delete")
	public String delete()
	{
		if(classInfo != null && classInfo.getcId() > 0)
		{
			System.out.println("jinlai l");
			classInfoService.delete(classInfo.getcId());
		}
		else
		{
			System.out.println("删除失败+++++++++++++");
		}
		return "save";
	}
	
	
	@Action("edit")
	public String edit()
	{
		if(classInfo != null)
		{
			classInfo = classInfoService.findById(classInfo.getcId());
		}
		return "add";
	}



	@Override
	public ClassInfo getModel() {
		// TODO Auto-generated method stub
		return classInfo;
	}

	public Integer getcId() {
		return cId;
	}

	public void setcId(Integer cId) {
		this.cId = cId;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}
	
	
	
	
}
