package cn.edu.mju.service;

import java.util.List;

import cn.edu.mju.model.ClassInfo;
import cn.edu.mju.utils.Pager;

public interface IClassInfoService {
	
	public void save(ClassInfo c);
	public void delete(Integer cId);
	public ClassInfo findById(Integer cId);
	public List<ClassInfo> findAll(ClassInfo c);
	public Pager findAll(Integer page,ClassInfo c);

}
