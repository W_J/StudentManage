package cn.edu.mju.service;

import java.util.List;

import cn.edu.mju.model.UserGroup;
import cn.edu.mju.utils.Pager;

public interface IUserGroupService {
	public void save(UserGroup instance);
	public void deleteById(Integer id);
	public Pager findAll(Integer page,UserGroup instance);
	public UserGroup findById(Integer id);
	public List<UserGroup> findAll(UserGroup instance);
	
}
