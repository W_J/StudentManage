package cn.edu.mju.service.impl;

import java.util.List;

import cn.edu.mju.dal.IClassInfoDAO;
import cn.edu.mju.dal.impl.ClassInfoDAO;
import cn.edu.mju.model.ClassInfo;
import cn.edu.mju.service.IClassInfoService;
import cn.edu.mju.utils.Pager;

public class ClassInfoService implements IClassInfoService{
	
	private IClassInfoDAO classInfoDAO = new ClassInfoDAO();
	
	
	@Override
	public void save(ClassInfo c) {
		// TODO Auto-generated method stub
		classInfoDAO.save(c);
	}

	@Override
	public void delete(Integer cId) {
		// TODO Auto-generated method stub
		classInfoDAO.delete(cId);
	}

	@Override
	public ClassInfo findById(Integer cId) {
		// TODO Auto-generated method stub
		return classInfoDAO.findById(cId);
	}

	@Override
	public List<ClassInfo> findAll(ClassInfo c) {
		// TODO Auto-generated method stub
		return classInfoDAO.findAll(c);
	}

	@Override
	public Pager findAll(Integer page, ClassInfo c) {
		// TODO Auto-generated method stub
		return classInfoDAO.findAll(page, c);
	}

}
