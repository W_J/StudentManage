package cn.edu.mju.service.impl;

import java.util.List;

import cn.edu.mju.dal.IStudentDAO;
import cn.edu.mju.dal.impl.StudentDAO;
import cn.edu.mju.model.Student;
import cn.edu.mju.service.IStudentService;
import cn.edu.mju.utils.Pager;

public class StudentService implements IStudentService{
	
	private IStudentDAO studentDAO = new StudentDAO();	
	@Override
	public void save(Student s) {
		studentDAO.save(s);
		
	}

	@Override
	public Pager findAll(Integer page, Student s) {
		
		return studentDAO.findAll(page, s);
	}

	@Override
	public void delete(Integer sId) {
		
		studentDAO.delete(sId);
	}

	@Override
	public Student findById(Integer sId) {
		
		return studentDAO.findById(sId);
	}

	@Override
	public List<Student> findAll(Student s) {
		// TODO Auto-generated method stub
		return studentDAO.findAll(s);
	}

}
