package cn.edu.mju.service.impl;

import java.util.List;

import cn.edu.mju.dal.IUserGroupDAO;
import cn.edu.mju.dal.impl.UserDAO;
import cn.edu.mju.dal.impl.UserGroupDAO;
import cn.edu.mju.model.UserGroup;
import cn.edu.mju.utils.Pager;

public class UserGroupService implements cn.edu.mju.service.IUserGroupService {
	private IUserGroupDAO userGroupDAO = new UserGroupDAO();
	@Override
	public void save(UserGroup instance) {
		// TODO Auto-generated method stub
		userGroupDAO.save(instance);
	}
	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		userGroupDAO.delete(id);
	}
	@Override
	public Pager findAll(Integer page,UserGroup instance) {
		return userGroupDAO.findAll(page,instance);
	}
	@Override
	public UserGroup findById(Integer id) {
		// TODO Auto-generated method stub
		return userGroupDAO.findById(id);
	}
	@Override
	public List<UserGroup> findAll(UserGroup instance) {
		// TODO Auto-generated method stub
		return userGroupDAO.findAll(instance);
	}

}
