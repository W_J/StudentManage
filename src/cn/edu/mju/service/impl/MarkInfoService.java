package cn.edu.mju.service.impl;

import java.util.List;

import cn.edu.mju.dal.IMarkInfoDAO;
import cn.edu.mju.dal.impl.MarkInfoDAO;
import cn.edu.mju.model.MarkInfo;
import cn.edu.mju.service.IMarkInfoService;
import cn.edu.mju.utils.Pager;

public class MarkInfoService implements IMarkInfoService {

	private IMarkInfoDAO markInfoDAO = new MarkInfoDAO();
	@Override
	public void save(MarkInfo m) {
		// TODO Auto-generated method stub
		markInfoDAO.save(m);
	}

	@Override
	public Pager findPager(Integer page, MarkInfo m) {
		// TODO Auto-generated method stub
		return markInfoDAO.findPager(page, m);
	}

	@Override
	public void delete(Integer mId) {
		// TODO Auto-generated method stub
		markInfoDAO.delete(mId);
	}

	@Override
	public MarkInfo findById(Integer mId) {
		// TODO Auto-generated method stub
		return markInfoDAO.findById(mId);
	}

	@Override
	public List<MarkInfo> findAll(MarkInfo m) {
		// TODO Auto-generated method stub
		return markInfoDAO.findAll(m);
	}

}
