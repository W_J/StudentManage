package cn.edu.mju.service.impl;

import java.util.List;

import cn.edu.mju.dal.ITeacherDAO;
import cn.edu.mju.dal.impl.TeacherDAO;
import cn.edu.mju.model.Teacher;
import cn.edu.mju.service.ITeacherService;
import cn.edu.mju.utils.Pager;

public class TeacherService implements ITeacherService {
	
	private ITeacherDAO teacherDAO = new TeacherDAO();
	@Override
	public void save(Teacher t) {
		teacherDAO.save(t);
		
	}

	@Override
	public void delete(Integer tId) {
		teacherDAO.delete(tId);
		
	}

	@Override
	public Pager findPager(Integer page, Teacher t) {
		return teacherDAO.findPager(page, t);
	}

	@Override
	public Teacher findById(Integer tId) {
		return teacherDAO.findById(tId);
	}

	@Override
	public List<Teacher> findAll(Teacher t) {
		
		return teacherDAO.findAll(t);
	}

}
