package cn.edu.mju.service.impl;

import java.util.List;

import cn.edu.mju.dal.ICourseDAO;
import cn.edu.mju.dal.impl.CourseDAO;
import cn.edu.mju.model.Course;
import cn.edu.mju.service.ICourseService;
import cn.edu.mju.utils.Pager;

public class CourseService implements ICourseService {

	private ICourseDAO courseDAO = new CourseDAO();
	
	@Override
	public void save(Course c) {
		courseDAO.save(c);
		
	}

	@Override
	public void delete(Integer cId) {
		courseDAO.delete(cId);
		
	}

	@Override
	public Pager findPager(Integer page, Course c) {
		
		return courseDAO.findPager(page, c);
	}

	@Override
	public Course findById(Integer cId) {
		// TODO Auto-generated method stub
		return courseDAO.findById(cId);
	}

	@Override
	public List<Course> findAll(Course c) {
		// TODO Auto-generated method stub
		return courseDAO.findAll(c);
	}

}
