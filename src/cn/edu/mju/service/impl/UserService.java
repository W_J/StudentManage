package cn.edu.mju.service.impl;

import java.util.List;

import cn.edu.mju.dal.IUserDAO;
import cn.edu.mju.dal.impl.UserDAO;
import cn.edu.mju.model.User;
import cn.edu.mju.service.IUserService;
import cn.edu.mju.utils.Pager;

public class UserService implements IUserService{

	private IUserDAO userDAO = new UserDAO();
	@Override
	public void save(User u) {
		userDAO.save(u);
	}

	@Override
	public Pager findAll(Integer page,User u) {
		// TODO Auto-generated method stub
		return userDAO.findAll(page,u);
	}

	@Override
	public void delete(Integer id) {
		userDAO.delete(id);
	}

	@Override
	public User findById(Integer id) {
		return userDAO.findById(id);
	}

	@Override
	public List<User> findName() {
		return userDAO.findName();
	}

	@Override
	public List<User> findPwd() {
		return userDAO.findPwd();
	}

}
