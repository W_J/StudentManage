package cn.edu.mju.service;

import java.util.List;

import cn.edu.mju.model.Teacher;
import cn.edu.mju.utils.Pager;

public interface ITeacherService {
	
	public void save(Teacher t);
	public void delete(Integer tId);
	public Pager findPager(Integer page,Teacher t);
	public Teacher findById(Integer tId);
	public List<Teacher> findAll(Teacher t);
}
