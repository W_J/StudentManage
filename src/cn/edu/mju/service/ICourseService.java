package cn.edu.mju.service;

import java.util.List;

import cn.edu.mju.model.Course;
import cn.edu.mju.utils.Pager;

public interface ICourseService {
		
	public void save(Course c);
	public void delete(Integer cId);
	public Pager findPager(Integer page,Course c);
	public Course findById(Integer cId);
	public List<Course> findAll(Course c);
}
