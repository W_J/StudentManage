package cn.edu.mju.froot.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;


	@ParentPackage("struts-default")
	@Namespace("/")
	@Results(value = { 
			@Result(name="news",location="/WEB-INF/pages/front/news.jsp") 
			})
	public class NewsAction extends ActionSupport {
		@Action("news")
		public String news()
		{
			return "news";
		}
}
