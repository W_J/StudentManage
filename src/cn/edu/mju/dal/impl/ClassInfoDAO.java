package cn.edu.mju.dal.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import cn.edu.mju.dal.IClassInfoDAO;
import cn.edu.mju.db.DbHelper;
import cn.edu.mju.model.ClassInfo;
import cn.edu.mju.utils.Pager;

public class ClassInfoDAO implements IClassInfoDAO{

	@Override
	public void save(ClassInfo c) {
		// TODO Auto-generated method stub
		Session session = DbHelper.getSession();
		Transaction ts = session.beginTransaction();
		if(c.getcId() != null && c.getcId() > 0)
		{
			session.update(c);
		}
		else
		{
			session.save(c);
		}
		ts.commit();
		session.close();
	}

	@Override
	public void delete(Integer cId) {
		// TODO Auto-generated method stub
		Session session = DbHelper.getSession();
		Transaction ts = session.beginTransaction();
		ClassInfo classInfo = session.get(ClassInfo.class, cId);
		session.delete(classInfo);
		ts.commit();
		session.close();
	}

	@Override
	public ClassInfo findById(Integer id) {
		// TODO Auto-generated method stub
		Session session = DbHelper.getSession();
		String hql = "from ClassInfo where id=:id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		ClassInfo classInfo = (ClassInfo) query.getSingleResult();
		session.close();
		return classInfo;
	}

	@Override
	public List<ClassInfo> findAll(ClassInfo c) {
		Session session = DbHelper.getSession();
		String hql = new String("from ClassInfo");  //数据库数据查找到列表
		Query query = session.createQuery(hql);
		List<ClassInfo> list = query.list();
		session.close();
		return list;
	}

	@Override
	public Pager findAll(Integer page, ClassInfo c) {
		Session session = DbHelper.getSession();		
		//2、hql查询
		String hqlTotal = "select count(*) from ClassInfo where 1=1";
		String hqlSelect = "from ClassInfo order by id desc";
		
		
		StringBuffer hqlWhere = new StringBuffer("");    //UserGroup类名
		if(c.getDepartment() != null && !"".equals(c.getDepartment()))
		{
			hqlWhere.append("and department like :department ");
		}
		if(c.getGrade() != null && !"".equals(c.getGrade()))
		{
			hqlWhere.append("and grade like :grade ");
		}
		//找出数据总数
		Query query = session.createQuery(hqlTotal + hqlWhere.toString());
		
		//模糊查询
		if(c != null && c.getDepartment() != null && !"".equals(c.getDepartment()))
		{
			query.setString("department","%" + c.getDepartment() + "%");
		}
		if(c != null && c.getGrade() != null && !"".equals(c.getGrade()))
		{
			query.setString("grade","%" + c.getGrade() + "%");
		}
		int rows = ((Long) query.getSingleResult()).intValue();		//条数
		Pager pager = new Pager();			//实例化翻页的方法 （调用Pager方法）
		pager.setRows(rows);
		pager.setPage(page);
		
		query = session.createQuery(hqlSelect + hqlWhere.toString());	//查询UserGroup数据,显示在列表
		if(c.getDepartment() != null && !"".equals(c.getDepartment()))
		{
			query.setString("department","%" + c.getDepartment() + "%");
		}
		if(c.getGrade() != null && !"".equals(c.getGrade()))
		{
			query.setString("grade","%" + c.getGrade() + "%");
		}
		query.setFirstResult((pager.getPage()-1)* pager.getPageSize());
		query.setMaxResults(pager.getPageSize());
		pager.setData(query.list());
		//pager.getPages();
		session.close();
		return pager;
	}

}
