package cn.edu.mju.dal.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import cn.edu.mju.dal.IUserDAO;
import cn.edu.mju.db.DbHelper;
import cn.edu.mju.model.User;
import cn.edu.mju.utils.Pager;

public class UserDAO implements IUserDAO{

	@Override
	public void save(User u) {
		//构建一个SessionFactory实例，创建Session实例
		Session session = DbHelper.getSession();
				
		//由Session实例创建一格Transaction的一格实例，开启事务
		Transaction ts = session.beginTransaction();
		if(u.getId() != null && u.getId() > 0)
		{
			session.update(u);
		}
		else
		{
		//通过Session接口提供的各种方法操作数据库
			session.save(u);
		}		
		//提交或回滚事务:ts.commit()/ts.rollback
		ts.commit();
				
		//关闭Session
		session.close();		
	}

	@Override
	public Pager findAll(Integer page,User u) {
		// TODO Auto-generated method stub
		Session session = DbHelper.getSession();
		//String hql = new String("from User");  //数据库数据查找到列表
		String hqlTotal = "select count(*) from User where 1=1";
		String hqlSelect = "from User order by id desc";
		//通过条件查询数据
		StringBuffer hqlWhere = new StringBuffer("");
		if(u.getUserName() != null && !"".equals(u.getUserName()))
		{
			hqlWhere.append("and userName like :userName");
		}
		if(u.getEmail() != null && !"".equals(u.getEmail()))
		{
			hqlWhere.append("and email like :email");
		}
		Query query = session.createQuery(hqlTotal + hqlWhere.toString());
		
		if(u.getUserName() != null && !"".equals(u.getUserName()))
		{
			query.setString("userName","%" + u.getUserName() + "%");
		}
		if(u.getEmail() != null && !"".equals(u.getEmail()))
		{
			query.setString("email","%" + u.getEmail() + "%");
		}
		int rows = ((Long) query.getSingleResult()).intValue();		//条数
		Pager pager = new Pager();			//实例化翻页的方法 （调用Pager方法）
		pager.setRows(rows);
		pager.setPage(page);		
		query = session.createQuery(hqlSelect + hqlWhere.toString());	//查询User数据,显示在列表
		if(u.getUserName() != null && !"".equals(u.getUserName()))
		{
			query.setString("userName","%" + u.getUserName() + "%");
		}
		if(u.getEmail() != null && !"".equals(u.getEmail()))
		{
			query.setString("email","%" + u.getEmail() + "%");
		}
		query.setFirstResult((pager.getPage()-1)* pager.getPageSize());
		query.setMaxResults(pager.getPageSize());
		pager.setData(query.list());
		//pager.getPages();
		session.close();
		return pager;
	}

	@Override
	public void delete(Integer id) {
		Session session = DbHelper.getSession();
		Transaction ts = session.beginTransaction();
		User user = session.get(User.class, id);
		session.delete(user);
		ts.commit();
		session.close();
	}

	@Override
	public User findById(Integer id) {
		Session session = DbHelper.getSession();
		String hql = "from User where id=:id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		User user = (User) query.getSingleResult();
		session.close();
		return user;
	}

	@Override
	public List<User> findName() {
		Session session = DbHelper.getSession();
		Transaction ts  = session.beginTransaction();
		Query query = session.createQuery("select userName from User");
		List<User> lst = query.list();
		return lst;
	}

	@Override
	public List<User> findPwd() {
		Session session = DbHelper.getSession();
		Transaction ts  = session.beginTransaction();
		Query query = session.createQuery("select userPwd from User");
		List<User> lst = query.list();
		return lst;
	}

}
