package cn.edu.mju.dal.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import cn.edu.mju.dal.IUserGroupDAO;
import cn.edu.mju.db.DbHelper;
import cn.edu.mju.model.User;
import cn.edu.mju.model.UserGroup;
import cn.edu.mju.utils.Pager;

public class UserGroupDAO implements IUserGroupDAO {

	@Override
	public void save(UserGroup instance) {
		
		//构建一个SessionFactory实例，创建Session实例
		Session session = DbHelper.getSession();		
		//由Session实例创建一格Transaction的一格实例，开启事务
		Transaction ts = session.beginTransaction();
		if(instance.getId() != null && instance.getId() > 0)
		{
			session.update(instance);
		}
		else
		{
			//通过Session接口提供的各种方法操作数据库
			session.save(instance);
		}		
		//提交或回滚事务:ts.commit()/ts.rollback
		ts.commit();
		
		//关闭Session
		session.close();
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		//构建一个SessionFactory实例，创建Session实例
		Session session = DbHelper.getSession();
				
		//由Session实例创建一格Transaction的一格实例，开启事务
		Transaction ts = session.beginTransaction();
		UserGroup userGroup = session.get(UserGroup.class,id);		
		//通过Session接口提供的各种方法操作数据库
		session.delete(userGroup);
				
		//提交或回滚事务:ts.commit()/ts.rollback
		ts.commit();
		//关闭Session
		session.close();
	}

	@Override
	public Pager findAll(Integer page,UserGroup instance) {
		// TODO Auto-generated method stub
		//构建一个SessionFactory实例，创建Session实例
		Session session = DbHelper.getSession();		
		//2、hql查询
		String hqlTotal = "select count(*) from UserGroup where 1=1";
		String hqlSelect = "from UserGroup order by id desc";
		
		
		StringBuffer hqlWhere = new StringBuffer("");    //UserGroup类名
		if(instance.getName() != null && !"".equals(instance.getName()))
		{
			hqlWhere.append("and name like :name ");
		}		
		//找出数据总数
		Query query = session.createQuery(hqlTotal + hqlWhere.toString());
		
		//模糊查询
		if(instance != null && instance.getName() != null && !"".equals(instance.getName()))
		{
			query.setString("name","%" + instance.getName() + "%");
		}
		
		int rows = ((Long) query.getSingleResult()).intValue();		//条数
		Pager pager = new Pager();			//实例化翻页的方法 （调用Pager方法）
		pager.setRows(rows);
		pager.setPage(page);
		
		query = session.createQuery(hqlSelect + hqlWhere.toString());	//查询UserGroup数据,显示在列表
		if(instance.getName() != null && !"".equals(instance.getName()))
		{
			query.setString("name","%" + instance.getName() + "%");
		}
		query.setFirstResult((pager.getPage()-1)* pager.getPageSize());
		query.setMaxResults(pager.getPageSize());
		pager.setData(query.list());
		//pager.getPages();
		session.close();
		return pager;
		
	}

	@Override
	public UserGroup findById(Integer id) {
		Session session = DbHelper.getSession();
		String hql = "from UserGroup where id=:id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		UserGroup userGroup = (UserGroup) query.getSingleResult();
		session.close();
		return userGroup;
	}

	@Override
	public List<UserGroup> findAll(UserGroup instance) {
		// TODO Auto-generated method stub
		Session session = DbHelper.getSession();
		String hql = new String("from UserGroup");  //数据库数据查找到列表
		Query query = session.createQuery(hql);
		List<UserGroup> list = query.list();
		session.close();
		return list;
	}


	
}
