package cn.edu.mju.dal.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import cn.edu.mju.dal.IStudentDAO;
import cn.edu.mju.db.DbHelper;
import cn.edu.mju.model.Student;
import cn.edu.mju.utils.Pager;

public class StudentDAO implements IStudentDAO{

	@Override
	public void save(Student s) {
		// TODO Auto-generated method stub
		Session session = DbHelper.getSession();
		Transaction ts = session.beginTransaction();
		if(s.getsId() != null && s.getsId() > 0)
		{
			session.update(s);
		}
		else
		{
			session.save(s);
		}
		ts.commit();
		session.close();
	}

	@Override
	public Pager findAll(Integer page, Student s) {
		Session session = DbHelper.getSession();		
		//2、hql查询
		String hqlTotal = "select count(*) from Student where 1=1";
		String hqlSelect = "from Student order by id desc";
		
		
		StringBuffer hqlWhere = new StringBuffer("");    //UserGroup类名
		if(s.getStuNo() != null && !"".equals(s.getStuNo()))
		{
			hqlWhere.append("and stuNo like :stuNo ");
		}		
		//找出数据总数
		Query query = session.createQuery(hqlTotal + hqlWhere.toString());
		
		//模糊查询
		if(s.getStuNo() != null && !"".equals(s.getStuNo()))
		{
			query.setString("stuNo","%" + s.getStuNo() + "%");
		}
		
		int rows = ((Long) query.getSingleResult()).intValue();		//条数
		Pager pager = new Pager();			//实例化翻页的方法 （调用Pager方法）
		pager.setRows(rows);
		pager.setPage(page);
		
		query = session.createQuery(hqlSelect + hqlWhere.toString());	//查询UserGroup数据,显示在列表
		if(s.getStuNo() != null && !"".equals(s.getStuNo()))
		{
			query.setString("stuNo","%" + s.getStuNo() + "%");
		}
		query.setFirstResult((pager.getPage()-1)* pager.getPageSize());
		query.setMaxResults(pager.getPageSize());
		pager.setData(query.list());
		//pager.getPages();
		session.close();
		return pager;

	}

	@Override
	public void delete(Integer sId) {
		Session session = DbHelper.getSession();
		Transaction ts = session.beginTransaction();
		Student student = session.get(Student.class, sId);
		session.delete(student);
		ts.commit();
		session.close();
		
	}

	@Override
	public Student findById(Integer sId) {
		Session session = DbHelper.getSession();
		String hql = "from Student where sId=:sId";
		Query query = session.createQuery(hql);
		query.setInteger("sId", sId);
		Student student = (Student) query.getSingleResult();
		session.close();
		return student;
	}

	@Override
	public List<Student> findAll(Student s) {
		Session session = DbHelper.getSession();
		String hql = new String("from Student");
		Query query = session.createQuery(hql);
		List<Student> list = query.list();
		session.close();
		return list;
	}

}
