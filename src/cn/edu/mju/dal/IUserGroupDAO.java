package cn.edu.mju.dal;

import java.util.List;

import cn.edu.mju.model.UserGroup;
import cn.edu.mju.utils.Pager;

public interface IUserGroupDAO {
	public void save(UserGroup instance);
	public void delete(Integer id);
	public Pager findAll(Integer page,UserGroup instance);
	public UserGroup findById(Integer id);
	public List<UserGroup> findAll(UserGroup instance);
}
