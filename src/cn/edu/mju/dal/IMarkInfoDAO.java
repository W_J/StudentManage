package cn.edu.mju.dal;

import java.util.List;

import cn.edu.mju.model.MarkInfo;
import cn.edu.mju.utils.Pager;

public interface IMarkInfoDAO {
	
	
	public void save(MarkInfo m);
	public Pager findPager(Integer page,MarkInfo m);
	public void delete(Integer mId);
	public MarkInfo findById(Integer mId);
	public List<MarkInfo> findAll(MarkInfo m);
}
