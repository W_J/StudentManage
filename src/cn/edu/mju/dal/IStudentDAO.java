package cn.edu.mju.dal;

import java.util.List;

import cn.edu.mju.model.Student;
import cn.edu.mju.utils.Pager;

public interface IStudentDAO {
	
	public void save(Student s);
	public Pager findAll(Integer page,Student s);
	public void delete(Integer sId);
	public Student findById(Integer sId);
	public List<Student> findAll(Student s);
}
