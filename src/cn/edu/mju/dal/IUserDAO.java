package cn.edu.mju.dal;

import java.util.List;

import cn.edu.mju.model.User;
import cn.edu.mju.utils.Pager;

public interface IUserDAO {
	public void save(User u);
	public Pager findAll(Integer page,User u);
	public void delete(Integer id);
	public User findById(Integer id);
	public List<User> findName();
	public List<User> findPwd();
}
